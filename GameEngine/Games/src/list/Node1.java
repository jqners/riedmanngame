package list;

public class Node1 {

    private int value;
    private Node1 next;

    public Node1(int value){
        this.value = value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setNext(Node1 next) {
        this.next = next;
    }

    public int getValue() {
        return value;
    }

    public Node1 getNext() {
        return next;
    }
}
