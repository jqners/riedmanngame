package observer;

public abstract class AbstractMessObserver implements MessObserver{

    protected int serialNumber;

    public AbstractMessObserver(int serialNumber) {
        this.serialNumber = serialNumber;
    }

    @Override
    public int getSerialnumber() {
        return this.serialNumber;
    }
}
