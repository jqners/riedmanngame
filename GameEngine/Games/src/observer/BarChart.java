package observer;

public class BarChart extends AbstractMessObserver{

    public BarChart(int serialNumber) {
        super(serialNumber);
    }

    @Override
    public void drawDiagram(int x) {
        System.out.println("Bar Chart: " + serialNumber);
    }
}
