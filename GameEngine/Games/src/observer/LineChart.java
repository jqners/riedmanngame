package observer;

public class LineChart extends AbstractMessObserver{


    public LineChart(int serialNumber) {
        super(serialNumber);
    }

    @Override
    public void drawDiagram(int x) {
        System.out.println("Line Chart: " + serialNumber);

    }
}
