package observer;

public interface MessObserver {

    void drawDiagram(int x);
    int getSerialnumber();
}
