package observer;

import java.util.ArrayList;
import java.util.List;

public class Messgerät {

    private List <MessObserver> observerList;

    public Messgerät(){
        this.observerList = new ArrayList<>();
    }

    void drawDiagram(int x){
        for (MessObserver messObserver: observerList){
            messObserver.drawDiagram(x);
        }
    }

    void addObserver(MessObserver messObserver){
        this.observerList.add(messObserver);
    }
}
