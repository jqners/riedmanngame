package strategypatternV2;

public interface Acceleration {
    String myAcceleration(String acceleration);
}
