package strategypatternV2;

public class Equal implements Acceleration{
    @Override
    public String myAcceleration(String acceleration) {
        String myAcceleration = "Equal" + acceleration;
        return myAcceleration;
    }
}
