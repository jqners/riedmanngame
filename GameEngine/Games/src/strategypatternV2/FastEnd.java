package strategypatternV2;

public class FastEnd implements Acceleration{
    @Override
    public String myAcceleration(String acceleration) {
        String myAcceleration = "FastEnd" + acceleration;
        return myAcceleration;
    }
}
