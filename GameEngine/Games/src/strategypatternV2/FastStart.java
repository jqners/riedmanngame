package strategypatternV2;

public class FastStart implements Acceleration{
    @Override
    public String myAcceleration(String acceleration) {
        String myAcceleration = "FastStart" + acceleration;
        return myAcceleration;
    }
}
