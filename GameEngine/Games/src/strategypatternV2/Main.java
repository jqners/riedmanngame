package strategypatternV2;

public class Main {

    public static void main(String[] args) {

        FastStart FS1 = new FastStart();
        FastEnd FE1 = new FastEnd();
        Equal E1 = new Equal();

        Car c1 = new Car(" ist Car 1", FS1);
        Motorbike m1 = new Motorbike(" ist Motorbike 1", FS1);
        Car c2 = new Car(" ist Car 2", FE1);
        Motorbike m2 = new Motorbike(" ist Motorbike 2", E1);

        c1.GETACC();
        m1.GETACC();
        c2.GETACC();
        m2.GETACC();

    }
}
