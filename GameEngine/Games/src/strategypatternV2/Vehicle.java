package strategypatternV2;

public class Vehicle {

    private String acc = "";
    private Acceleration acceleration;

    public Vehicle(String acc, Acceleration acceleration) {
        this.acc = acc;
        this.acceleration = acceleration;
    }

    public void GETACC(){
        System.out.println(acceleration.myAcceleration(acc));
    }
}
