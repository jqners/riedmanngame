package strategypatterneins;

public class GUI {

    private String text = "";
    private Encrypter encrypter;

    public GUI(String text, Encrypter encrypter) {
        this.text = text;
        this.encrypter = encrypter;
    }

    public void Encrypt(){
        System.out.println(encrypter.Encrypt(text));
    }
}
