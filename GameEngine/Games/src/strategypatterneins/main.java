package strategypatterneins;

public class main {

    public static void main(String[] args) {

        Reverse R1 = new Reverse();
        Caesar C1 = new Caesar();

        GUI GUI1 = new GUI("Der Bratan bleibt der Gleiche", R1);
        GUI GUI2 = new GUI("Der Caesar bleibt der Gleiche", C1);

        GUI1.Encrypt();
        GUI2.Encrypt();

    }

}
