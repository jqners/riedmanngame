package strategypatternzwei;

public class Main {

    public static void main(String[] args) {

        FastStart fs1 = new FastStart();
        FastEnd fe1 = new FastEnd();
        Equal e1 = new Equal();

        Motorbike m1 = new Motorbike("Der Accelerationtype von Motorrad 1 ist: ", fs1);
        Car c1 = new Car("Der Accelerationtype von Auto 1 ist: ", fs1);
        Motorbike m2 = new Motorbike("Der Accelerationtype von Motorrad 2 ist: ", fe1);
        Car c2 = new Car("Der Accelerationtype von Auto 2 ist: ", e1);

        m1.getAcceleration();
        c1.getAcceleration();
        m2.getAcceleration();
        c2.getAcceleration();
    }
}
