package strategypatternzwei;

public class Motorbike extends Vehicle{

    public Motorbike(String acc, Acceleration acceleration) {
        super(acc, acceleration);
    }
}
