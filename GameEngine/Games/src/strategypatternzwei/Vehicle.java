package strategypatternzwei;

public class Vehicle {

    private String acc = "";
    private Acceleration acceleration;

    public Vehicle(String acc, Acceleration acceleration) {
        this.acc = acc;
        this.acceleration = acceleration;
    }

    public void getAcceleration(){
        System.out.println(acceleration.Accelerate(acc));
    }
}
