package at.nat.ObserverPatternTestExample;

public class Main {

    public static void main(String[] args) {

        Thermometer t1 = new Thermometer();
        Heizregler h1 = new Heizregler();
        Heizregler h2 = new Heizregler();
        Markiesenregler m1 = new Markiesenregler();
        Markiesenregler m2 = new Markiesenregler();

        t1.addObserver(h1);
        t1.addObserver(h2);
        t1.addObserver(m1);
        t1.addObserver(m2);

        h1.increaseTemperature(5);
        h2.increaseTemperature(7);
        m1.increaseTemperature(15);

        t1.notifyAllObservers();

    }
}
