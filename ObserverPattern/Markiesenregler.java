package at.nat.ObserverPatternTestExample;

public class Markiesenregler implements Observer {

    public static int currentTemperature = 0;

    public void increaseTemperature(int temperature) {
        currentTemperature += temperature;
    }

    public void notifySensor() {
        System.out.println("Sensor notified. Temperature: " + currentTemperature);
    }
}
