package at.nat.ObserverPatternTestExample;

public interface Observer {

    public void increaseTemperature(int temperature);
    public void notifySensor();

}
