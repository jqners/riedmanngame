package at.nat.ObserverPatternTestExample;
import java.util.ArrayList;

public class Thermometer {

    private ArrayList<Observer> observers = new ArrayList<Observer>();

    public void addObserver(Observer observer){
        observers.add(observer);
    }

    public void notifyAllObservers(){
        for(Observer observer : observers){
            observer.notifySensor();
        }
    }

}
